import matplotlib.pyplot
import matplotlib.animation
import matplotlib.pylab
import scipy.optimize
import marketapi

class Bot:
    """
    Главная логика бота
    """
    def __init__(self, big_period: int, medium_period: int, small_period: int,
                 show_main_trend: bool = False,
                 show_additional_trends: bool = False,
                 show_maxmin_trends: bool = False):
        """
        Инициализация бота
        """
        self.big_period = big_period
        self.medium_period = medium_period
        self.small_period = small_period
        if show_main_trend or show_additional_trends or show_maxmin_trends:
            matplotlib.pyplot.ion()
            matplotlib.pyplot.show()
        self.show_main_trend = show_main_trend
        self.show_additional_trends = show_additional_trends
        self.show_maxmin_trends = show_maxmin_trends

    def update_graphic(self, price: list, curve_big: list, curve_medium: list, curve_small: list, tl_min: list, tl_max: list) -> None:
        if not self.show_main_trend and not self.show_additional_trends and not self.show_maxmin_trends:
            return

        matplotlib.pyplot.gcf().clear()
        matplotlib.pyplot.plot(price[0], price[1], label="Price")
        if self.show_main_trend:
            matplotlib.pyplot.plot(curve_big[0], curve_big[1], label="Big Trend (K={0})".format(round(curve_big[2], 3)))
        if self.show_additional_trends:
            matplotlib.pyplot.plot(curve_medium[0], curve_medium[1], label="Medium Trend (K={0})".format(round(curve_medium[2], 3)))
            matplotlib.pyplot.plot(curve_small[0], curve_small[1], label="Small Trend (K={0})".format(round(curve_small[2], 3)))
        if self.show_maxmin_trends:
            matplotlib.pyplot.plot(tl_min[0], tl_min[1], label="Minimum Trend Line (K={0})".format(round(tl_min[2], 3)))
            matplotlib.pyplot.plot(tl_max[0], tl_max[1], label="Maximum Trend Line (K={0})".format(round(tl_max[2], 3)))
        matplotlib.pyplot.legend()
        matplotlib.pyplot.draw()
        matplotlib.pyplot.pause(0.001)

    def on_price_updated(self, mapi: 'marketapi.MarketApi') -> None:
        """
        Вызывается при поступлении рыночных данных для принятия решения на их основании
        :param mapi: api для взаимодействия
        """

        if len(mapi.get_prices()) < self.big_period:
            return

        """Анализ трендовых с разными периодами"""

        def fun_linear(x, a, b):
            return a * x + b

        x_data_big = list(range(self.big_period))
        y_data_big = mapi.get_prices()[-self.big_period:]
        p_opt_big, _ = scipy.optimize.curve_fit(fun_linear, x_data_big, y_data_big)
        big_curve_data = []
        for x in x_data_big:
            big_curve_data.append(fun_linear(x, p_opt_big[0], p_opt_big[1]))

        x_data_medium = x_data_big[-self.medium_period:]
        y_data_medium = mapi.get_prices()[-self.medium_period:]
        p_opt_medium, _ = scipy.optimize.curve_fit(fun_linear, x_data_medium, y_data_medium)
        curve_data_medium = []
        for x in x_data_medium:
            curve_data_medium.append(fun_linear(x, p_opt_medium[0], p_opt_medium[1]))

        x_data_small = x_data_big[-self.small_period:]
        y_data_small = mapi.get_prices()[-self.small_period:]
        p_opt_small, _ = scipy.optimize.curve_fit(fun_linear, x_data_small, y_data_small)
        curve_data_small = []
        for x in x_data_small:
            curve_data_small.append(fun_linear(x, p_opt_small[0], p_opt_small[1]))

        """Анализ верхней и нижней трендовой"""
        sorted_prices = sorted(zip(range(self.big_period), mapi.get_prices()[-self.big_period:]),
                               key=lambda item: item[1])
        minimum_prices = sorted_prices[:2]
        maximum_prices = sorted_prices[-2:]
        x_minimums, y_minimums = zip(*minimum_prices)
        x_maximus, y_maximus = zip(*maximum_prices)
        p_opt_minimums, _ = scipy.optimize.curve_fit(fun_linear, x_minimums, y_minimums)
        p_opt_maximus, _ = scipy.optimize.curve_fit(fun_linear, x_maximus, y_maximus)

        x_mm_vals = list(range(self.big_period))
        y_minimus_vals = []
        y_maximus_vals = []
        for x in x_mm_vals:
            y_minimus_vals.append(fun_linear(x, p_opt_minimums[0], p_opt_minimums[1]))
            y_maximus_vals.append(fun_linear(x, p_opt_maximus[0], p_opt_maximus[1]))

        self.update_graphic([x_data_big, y_data_big],
                            [x_data_big, big_curve_data, p_opt_big[0]],
                            [x_data_medium, curve_data_medium, p_opt_medium[0]],
                            [x_data_small, curve_data_small, p_opt_small[0]],
                            [x_mm_vals, y_minimus_vals, p_opt_minimums[0]],
                            [x_mm_vals, y_maximus_vals, p_opt_maximus[0]])
