import websocket
import json

import bot


class MarketApiException(Exception):
    """
    Исключения, выбрасываемые при работе с биржей
    """


class MarketApi:
    """
    API для работы с биржей
    """
    def __init__(self):
        """
        Инициплизирует API для работы с рынком
        """
        raise NotImplementedError()

    def run(self) -> None:
        """
        Запускает главный цикл работы с биржей
        """
        raise NotImplementedError()

    def get_prices(self) -> list:
        """
        Возвращает список цен
        :return: список цен
        """
        raise NotImplementedError()


class BitfinexApi(MarketApi):
    """
    API для работы с bitfinex
    """
    def __init__(self, bot: bot.Bot, trade_symbol: str, price_buffer_size: int):
        """
        Инициализирует API для работы с bitfinex
        """
        self.bot = bot
        self.prices = []
        self.prices_count = price_buffer_size
        self.maintenance = False
        self.trade_symbol = trade_symbol
        self.trade_symbol_channel_id = None
        self.ws = websocket.WebSocketApp('wss://api.bitfinex.com/ws/2',
                                         on_open=self.__on_bitfinex_open,
                                         on_message=self.__on_bitfinex_message,
                                         on_close=self.__on_bitfinex_close,
                                         on_error=self.__on_bitfinex_error)

    def run(self) -> None:
        """
        Запускает главный цикл работы с биржей
        """
        self.ws.run_forever()

    def get_prices(self) -> list:
        """
        Возвращает список цен
        :return: список цен
        """
        return self.prices

    def __on_bitfinex_open(self, ws: 'websocket.WebSocketApp') -> None:
        """
        Вызывается после установления соединения. Здесь следует выполнить авторизацию и подписаться на нужные каналы
        :param ws: websocket для использования
        :return:
        """
        ws.send(json.dumps({'event': "subscribe", 'channel': "trades", 'symbol': self.trade_symbol}))

    def __on_bitfinex_message(self, ws: 'websocket.WebSocketApp', raw_message: str) -> None:
        message = json.loads(raw_message)
        if 'event' in message:
            if message['event'] == 'info':
                if 'code' in message:
                    if message['code'] == 20051:
                        """Stop/Restart Websocket Server (please reconnect)"""
                        ws.close()
                        raise MarketApiException('Stop/Restart Websocket Server (please reconnect)')
                    elif message['code'] == 20060:
                        """
                        Entering in Maintenance mode. 
                        Please pause any activity and resume after receiving the info message 20061 
                        (it should take 120 seconds at most).
                        """
                        self.maintenance = True
                    elif message['code'] == 20061:
                        """
                        Maintenance ended. You can resume normal activity. 
                        It is advised to unsubscribe/subscribe again all channels.
                        """
                        self.maintenance = False
            elif message['event'] == 'subscribed':
                if message['channel'] == 'trades':
                    if message['symbol'] == self.trade_symbol:
                        self.trade_symbol_channel_id = message['chanId']
        else:
            if message[0] == self.trade_symbol_channel_id:
                if len(message) == 3:
                    """Update"""
                    if message[1] == 'te':
                        self.prices.append(message[2][3])
                        if len(self.prices) > self.prices_count:
                            self.prices.pop(0)
                        self.bot.on_price_updated(self)
                else:
                    """Snapshot"""
                    for price_item in sorted(message[1], key=lambda x: x[1], reverse=True):
                        self.prices.insert(0, price_item[3])
                        if len(self.prices) >= self.prices_count:
                            break
                    self.bot.on_price_updated(self)

    def __on_bitfinex_close(self, ws: 'websocket.WebSocketApp') -> None:
        raise MarketApiException('Market connection closed')

    def __on_bitfinex_error(self, error: str) -> None:
        raise MarketApiException(error)
